sipi-firefox:
  {% if grains['cpuarch'] == 'AMD64' %}
    {% set PROGRAM_FILES = "%ProgramFiles(x86)%" %}
  {% else %}
    {% set PROGRAM_FILES = "%ProgramFiles%" %}
  {% endif %}
  {% for version in ['42.0', '43.0', '43.0.1', '43.0.2', '43.0.3', '43.0.4', '44.0', '44.0.1', '44.0.2', '45.0', '45.0.1', '45.0.2', '46.0', '46.0.1', '47.0', '47.0.1', '48.0', '48.0.1'] %}
  '{{ version }}':
    full_name: 'Mozilla Firefox {{ version }} (x64 hu-HU)'
    installer: 'https://download-installer.cdn.mozilla.net/pub/firefox/releases/{{ version }}/win64/hu/Firefox%20Setup%20{{ version }}.exe'
    install_flags: '/s'
    uninstaller: '{{ PROGRAM_FILES }}\Mozilla Firefox\uninstall\helper.exe'
    uninstall_flags: '/S'
    msiexec: False
    locale: hu_HU
    reboot: False
  {% endfor %}
